## Java Spring API with Mongo 
The api creates a Mongo database with 3 entries: 
Employee_id: INT,
Employee_Name: String,
Employee_Designation: String.

The api offers 3 endpoints:
GET/getEmployee: To get all the employees
GET/getEmployee/{id}: To get a specific employee, given id
POST/addEmployee: To post employee details into the database
DELETE/deleteEmp/{id}: To delete employee, given employee id.

To run the file, we need to first build our docker container named 'spring_mongo' using:
docker build -t spring_mongo .

docker images: will reveal the active dcoker images

To get the api working, we need to run:
docker-compose up: this will launch the api which can be tested by running: 
http://localhost:8080/swagger-ui.html, in the browser.